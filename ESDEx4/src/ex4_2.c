//	set path=C:\MinGW\bin;%path%

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"

#include "BMP_huff.h"

#ifdef RUN_2

#define INPUT_IMG "./input/duck.bmp"		// input image (24 bits)
#define OUTPUT_IMG "./output/quantized.bmp"	// quantized image (4, 8 bits)
#define INPUT_Q "./output/quantized.bmp"	// input quantized image
#define OUTPUT_CP "./output/result_huffman.bmp"	// extracted image

int main(int argc, char * argv[])
{
	//============= quantizing an image ====================
	BMPIMG img;
	BMPIMG_initialize(&img);

	if(!BMPIMG_open(&img, INPUT_IMG)){
		printf("open file <%s> failed\n", INPUT_IMG);
		return 0;
	}

	// quantize to 4 bit
	int RQ = 2, GQ = 2, BQ = 4, bits = 4;
//	int RQ = 6, GQ = 7, BQ = 6, bits = 8;
	BMPIMG_create_colormap(&img, RQ, GQ, BQ, bits);
	BMPIMG_convert_to_colormap(&img, RQ, GQ, BQ, bits);

	BMPIMG_save(&img, OUTPUT_IMG);
	BMPIMG_destroy(&img);
	//=============== end quantizing =====================


	//=============== compressing quantized image ========
	FILE * fp;
	HINFO hinfo, hinfo2;
	BMPIMG img_q;
	BMPIMG_initialize(&img_q);

	// open quantized image
	if(!BMPIMG_open(&img_q, INPUT_Q)){
		printf("open file <%s> failed\n", INPUT_Q);
		return 0;
	}

	fp = fopen(INPUT_Q, "rb");
	hinfo.fp_bmp = fp;
	hinfo.img = &img_q;

	// prepare for compression
	HINFO_initialize(&hinfo);
	HINFO_count_symbols(&hinfo);
	HINFO_create_huffman_tree(&hinfo);
	HINFO_print_sorted_symbols(&hinfo);

	// write compressed file
	HINFO_write_compressed_file(&hinfo, OUTPUT_CP);

	// read compressed file, uncompress, and write extracted file
	HINFO_read_compressed_file(&hinfo2, OUTPUT_CP);

	fclose(fp);
	//================= end compressing ==========
	return 1;
}

#endif
