//	set path=C:\MinGW\bin;%path%

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "huff.h"
#include "config.h"

#define INPUT "./input/embedded_software_lecture_PDF.pdf.hmc"
#define OUTPUT "./output/embedded_software_lecture_PDF.pdf.hmc"

#ifdef RUN_1
void abort_program(const char * msg)
{
	printf("abort: (%s) is FALSE!\n", msg);
	exit(-1);
}

#include <math.h>

double log2(double n)
{
	return log(n) / log(2);
}

void swap3(void ** a0, void ** a1)
{
	void * t = *a0;
	*a0 = *a1;
	*a1 = t;
}

int partition(void * a[], int left, int right, int (* is_ordered)(void *, void *))
{
	void * p_pivot = a[right];
	int i = left, k = right;
	while(i < k){
		while(i < k && is_ordered(a[i], p_pivot)) i ++;
		while(i < k && !is_ordered(a[k], p_pivot)) k --;
		if(i < k) swap3(&a[i], &a[k]);
	}
	if(right > k) swap3(&a[right], &a[k]);
	return k;
}

void quick_sort(void * a[], int left, int right, int (* is_ordered)(void *, void *))
{
	if(left < right){
		int pivot_pos = partition(a, left, right, is_ordered);
		quick_sort(a, left, pivot_pos - 1, is_ordered);
		quick_sort(a, pivot_pos + 1, right, is_ordered);
	}
}

void SINFO_initialize(SINFO * sinfo, int i)
{
	sinfo->index = i;
	sinfo->count = 0;
	sinfo->left = 0;
	sinfo->right = 0;
	sinfo->parent = 0;
	sinfo->code = 0;
	sinfo->code_length = 0;
	sinfo->pos = 0;
}

void HINFO_initialize_p_sinfo(HINFO * hinfo)
{
	int i;
	for(i = 0; i < 256; i ++){
		hinfo->p_sinfo[i] = &hinfo->sinfo[i];
	}
}

void HINFO_initialize(HINFO * hinfo)
{
	int i;
	for(i = 0; i < 256; i ++){
		SINFO_initialize(&hinfo->sinfo[i], i);
		SINFO_initialize(&hinfo->inode[i], i);
	}
	hinfo->root_node = 0;
	hinfo->vertical_pitch = 1;
}

int SINFO_is_ordered(void * a, void * b)
{
	return ((SINFO *) a)->count > ((SINFO *) b)->count;
}

char * convert_char(int c)
{
	static char __str[3] = {0, 0, 0};
#define SET_STRING(c0, c1) __str[0] = c0; __str[1] = c1;
	switch(c){
		case '\a': SET_STRING('\\', 'a') break;
		case '\b': SET_STRING('\\', 'b') break;
		case '\f': SET_STRING('\\', 'f') break;
		case '\n': SET_STRING('\\', 'n') break;
		case '\r': SET_STRING('\\', 'r') break;
		case '\t': SET_STRING('\\', 't') break;
		case '\v': SET_STRING('\\', 'v') break;
		default: SET_STRING(c, 0); break;
	}
	return __str;
}

void HINFO_count_symbols(HINFO * hinfo)
{
	hinfo->text_count = 0;
	while(1){
		int c = fgetc(hinfo->fp_txt);
		if(c == EOF)
			break;
		ASSERT(c >= 0 && c < 256);
		hinfo->sinfo[c].count ++;
		hinfo->text_count ++;
	}
	printf("text_count = %d\n", hinfo->text_count);
}

void SINFO_print_huffman_code(SINFO * sinfo)
{
	int i, mask;
	printf("(%2d bits: ", sinfo->code_length);
	for(i = 0, mask = 1; i < sinfo->code_length; i ++, mask <<= 1)
		printf("%c", ((sinfo->code) & mask) ? '1' : '0');
	printf(" )");
}

void SINFO_create_sinfo_node(SINFO * child0, SINFO * child1, SINFO * parent)
{
	child0->parent = parent;
	child1->parent = parent;
	parent->left = child0;
	parent->right = child1;
	parent->count = child0->count + child1->count;
}

void SINFO_assign_code(SINFO * sinfo)
{
	if(sinfo->parent){
		sinfo->code = (SIB_DIR(sinfo, right) << sinfo->parent->code_length) + (sinfo->parent->code);
		sinfo->code_length = sinfo->parent->code_length + 1;
	}
	if(sinfo->left) SINFO_assign_code(sinfo->left);
	if(sinfo->right) SINFO_assign_code(sinfo->right);
}

void HINFO_sort_symbols(HINFO * hinfo)
{
	int i;
	HINFO_initialize_p_sinfo(hinfo);
	quick_sort((void **) hinfo->p_sinfo, 0, 255, SINFO_is_ordered);
	for(i = 255; i >= 0; i --){
		if(hinfo->p_sinfo[i]->count)
			break;
	}
	hinfo->symbol_count = i + 1;
}

void HINFO_create_huffman_tree(HINFO * hinfo)
{
	int tail, cindex = 0;
	HINFO_sort_symbols(hinfo);
	tail = hinfo->symbol_count - 1;
	while(tail > 0){
		SINFO * child0, * child1, * parent;
		child1 = hinfo->p_sinfo[tail];
		child0 = hinfo->p_sinfo[tail - 1];
		parent = &hinfo->inode[cindex ++];
		SINFO_create_sinfo_node(child0, child1, parent);
		hinfo->p_sinfo[tail - 1] = parent;
		quick_sort((void **) hinfo->p_sinfo, 0, tail - 1, SINFO_is_ordered);
		tail --;
	}
	ASSERT(cindex == hinfo->symbol_count - 1);
	hinfo->root_node = hinfo->p_sinfo[0];
	SINFO_assign_code(hinfo->root_node);
}

void HINFO_print_sorted_symbols(HINFO * hinfo)
{
	int i, total_bits = 0, total_bytes;
	HINFO_sort_symbols(hinfo);
	for(i = 0; i < hinfo->symbol_count; i ++){
		SINFO * sinfo = hinfo->p_sinfo[i];
		printf("%3d<%2s> : %5d ",
		sinfo->index, convert_char(sinfo->index), sinfo->count);
		SINFO_print_huffman_code(sinfo);
		printf("\n");
		total_bits += sinfo->code_length * sinfo->count;
	}
	total_bytes = (total_bits + 7) / 8;
	printf("total bits = %d\ntotal bytes = %d\n",
		total_bits, total_bytes);
	printf("compression rate = %f\n", (double) total_bytes / (double) hinfo->text_count);
}

int main(int argc, char * argv[])
{
	FILE * fp;
	HINFO hinfo, hinfo2;
	fp = fopen(INPUT, "rb");
	if(fp == 0){
		printf("cannot open file <%s>!!\n", INPUT);
		return 0;
	}

	int v_pitch = 1;
	if(v_pitch >= 1 && v_pitch <= 10)
		hinfo.vertical_pitch = v_pitch;

	hinfo.fp_txt = fp;

	HINFO_initialize(&hinfo);
	HINFO_count_symbols(&hinfo);
	HINFO_create_huffman_tree(&hinfo);
	HINFO_print_sorted_symbols(&hinfo);
	HINFO_write_compressed_file(&hinfo, OUTPUT);
	HINFO_read_compressed_file(&hinfo2, OUTPUT);
	fclose(fp);
	return 1;
}

#endif
