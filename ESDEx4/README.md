Exercise 4
===========

Name : Ratchasak Ranron

Student ID : 56-2204-0664

---

Answer
-----------------
###problem 4.1

* Observe the compression rate of different kinds of files (not text file)
	* I observed the compression of "embedded_software_lecture_PDF.pdf" file (in input directory) which is 0.992389, in other word "It's almost the same"
	* while the text file "cppinternals.info", compression rate is almost one half (0.572051).
* What happens when you compress an already compressed file?
	* This problem, I observe the compression rate of "embedded_software_lecture_PDF.pdf.hmc" which I compressed from the previous problem. Again!!, It's almost the same with 0.999858 compression rate.

###problem 4.2
* Try different quantization levels, especially observe what happens to the compression rate when you decrease colormap_size
	* the compression rate of 4 bits/pixel color quantization is 0.199626
	* the compression rate of 8 bits/pixel color quantization is 0.311637, from the same unquantized image.

Files description
------------------

In attached file, I include 2 folders named "src", "input" and "output"

* the "src" folder contains .c source file named "ex4_x.c" where x is the number of problem.
* the "input" folder contains files which is the input file to modify or observe for all problem.
	* "cppinternals.info" : used in problem 4.1
	* "embedded\_software\_lecture_PDF.pdf" : used in problem 4.1
	* "embedded\_software\_lecture_PDF.pdf.hmc" : the compressed file to be compress (in problem 4.1).
	* "duck.bmp" : used in problem 4.2 as an input image to quantize.
* the "output" folder contains .bmp file and .hmc file. 
	* "cppinternals.info.hmc" : the compressed file of "cppinternals.info"
	* "embedded\_software\_lecture\_PDF.pdf.hmc" : the compressed file of "embedded\_software\_lecture\_PDF.pdf"
	* "embedded\_software\_lecture\_PDF.pdf.hmc.hmc" : the compressed file of "embedded\_software\_lecture\_PDF.pdf.hmc"
	* "quantized\_xbits.bmp" : is the quantized image, where "x" is bit/pixel (4 and 8)
	* "result\_huffman_xbits.bmp.hmc" is the compressed file quantized image, where "x" is bit/pixel (4 and 8)
	* "result\_huffman_xbits.bmp" is the decompressed quantized image, where "x" is bit/pixel (4 and 8)

Note
------------------

I used "Eclipse" as an editor. I have to create a project (contains all problem solution) which can have only one main function.
So I used the "ifdef" pre-processor to select which main function I need to execute and control by change defined variable RUN_X in "config.h",
where X is the number of problem. for example if I want to run problem 7, I just change it to RUN_7.
