#include <stdio.h>
#include <stdlib.h>
#include "./config.h"

#ifdef RUN_7

#define OUTPUT_FILE "./output/ex1_7.txt"

FILE * fp;
void bubble_sort(int list[], int n);

int main(int argc, char* argv[]) {
	fp = fopen(OUTPUT_FILE, "a+");

	int i;
	int isPrintFirst = 1;
	int num[argc - 1];

	// print sequence of number and assign the numbers to num array
	fprintf(fp, "Sequence of numbers : ");
	for(i = 0; i < argc - 1; i++) {
		num[i] = atoi(argv[i + 1]);
		if(isPrintFirst) {
			fprintf(fp, "%d", num[i]);
			isPrintFirst = 0;
		}
		else fprintf(fp, " , %d", num[i]);
	}

	// sort the numbers
	bubble_sort(num, argc - 1);

	// print sorted number
	fprintf(fp, "\nSorted numbers : ");
	isPrintFirst = 1;
	for(i = 0; i < argc - 1; i++) {
		if(isPrintFirst) {
			fprintf(fp, "%d", num[i]);
			isPrintFirst = 0;
		}
		else fprintf(fp, " , %d", num[i]);
	}

	fprintf(fp, "\n");
	fclose(fp);
	return 0;
}

void bubble_sort(int list[], int n) {
  int i, j, temp;

  for (i = 0 ; i < ( n - 1 ); i++) {
    for (j = 0 ; j < n - i - 1; j++) {
      if (list[j] > list[j+1]) {
        /* Swapping */
    	temp      = list[j];
        list[j]   = list[j+1];
        list[j+1] = temp;
      }
    }
  }
}

#endif
