#include <stdio.h>
#include <stdlib.h>
#include "./config.h"

#ifdef RUN_4

#define OUTPUT_FILE "./output/ex1_4.txt"

FILE * fp;

int main(int argc, char* argv[]) {
	fp = fopen(OUTPUT_FILE, "a+");

	int multiplicand = atoi(argv[1]);
	int multiplier = atoi(argv[2]);

	int i;
	int product = 0;

	fprintf(fp, "%d multiplied by %d : ", multiplicand, multiplier);

	for(i = 0; i < (sizeof(multiplicand) * 8); i++) {
		if((multiplier & 1) == 1)
			product += multiplicand;

		multiplicand = multiplicand << 1;
		multiplier = multiplier >> 1;
	}

	fprintf(fp, "%d", product);

	fprintf(fp, "\n");
	fclose(fp);
	return 0;
}

#endif
