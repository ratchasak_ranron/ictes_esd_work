#include <stdio.h>
#include <stdlib.h>
#include "./config.h"

#ifdef RUN_6

#define OUTPUT_FILE "./output/ex1_6.txt"

FILE * fp;
int atoi(const char* str);

int main(int argc, char* argv[]) {
	fp = fopen(OUTPUT_FILE, "a+");

	fprintf(fp, "Converting string \"%s\" to integer : %d", argv[1], atoi(argv[1]));

	fprintf(fp, "\n");
	fclose(fp);
	return 0;
}

int atoi(const char* str) {
	int i;
	int result = 0;
	int isMinus = 0;

	for(i = 0; str[i] != '\0'; i++) { // skip null character at the last index
		int num = str[i] - '0';
		if(!(str[i] == '-' || (num >= 0 && num <= 9))) { // not a number
			printf("Error: input is not alpha-numeric string");
			exit(EXIT_FAILURE);
		}

		if(i == 0 && str[i] == '-') {
			isMinus = 1;
		} else {
			result = (result * 10) + num;
		}
	}

	if(isMinus) result *= -1;

	return result;
}

#endif
