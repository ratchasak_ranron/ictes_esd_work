#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "./config.h"

#ifdef RUN_3

#define OUTPUT_FILE "./output/ex1_3.txt"

FILE * fp;
void printPrimeFactoredForm(int N);

int main(int argc, char* argv[]) {
	fp = fopen(OUTPUT_FILE, "a+");

	int N = atoi(argv[1]);

	fprintf(fp, "%d = ", N);
	printPrimeFactoredForm(N);

	fprintf(fp, "\n");
	fclose(fp);
	return 0;
}

void printPrimeFactoredForm(int N) {
	int prime, divisor;
	int temp = N;
	int isPrintFirst = 1;

	for(prime = 2; prime <= N; prime++) {
		int isPrime = 1;
		for(divisor = 2; divisor <= sqrt(N); divisor++) {
			if(prime % divisor == 0 && prime != divisor) {
				isPrime = 0;
				break;
			}
		}
		if(isPrime) {
			while(temp % prime == 0) {
				if(isPrintFirst) {
					fprintf(fp, "%d", prime);
					isPrintFirst = 0;
				} else {
					fprintf(fp, " * %d", prime);
				}
				temp /= prime;
			}
		}
	}
}

#endif
