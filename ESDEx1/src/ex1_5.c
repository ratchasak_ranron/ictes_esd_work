#include <stdio.h>
#include <stdlib.h>
#include "./config.h"

#ifdef RUN_5

#define OUTPUT_FILE "./output/ex1_5.txt"

FILE * fp;
void printBinary(int num);
int twoComplement(int n);
int getMSB(int n);

int main(int argc, char* argv[]) {
	fp = fopen(OUTPUT_FILE, "a+");

	int quotient = 0;

	int dividend = atoi(argv[1]);
	int divisor = atoi(argv[2]);

	fprintf(fp, "%d divided by %d : ", dividend, divisor);

	// get msb of dividend and divisor
	int msbDividend = getMSB(dividend);
	int msbDivisor = getMSB(divisor);

	// convert dividend and divisor to positive number
	if(msbDividend == 1) dividend = twoComplement(dividend);
	if(msbDivisor == 1) divisor = twoComplement(divisor);

	int cmpPortion = 0;
	int i;
	for(i = 0; i < (sizeof(dividend) * 8); i++) {
		// add msb of divided to the right of cmpPortion
		cmpPortion = (cmpPortion << 1) | getMSB(dividend);
		dividend = dividend << 1;

		if(cmpPortion >= divisor) {
			cmpPortion -= divisor;

			// add 1 to the right of quotient
			quotient = (quotient << 1) | 1;
		} else {
			// add 0 to the right of quotient
			quotient = quotient << 1;
		}
	}

	if(msbDividend != msbDivisor) quotient = twoComplement(quotient);

	// portion is the remainder
	fprintf(fp, "quotient = %d, remainder = %d", quotient, cmpPortion);

	fprintf(fp, "\n");
	fclose(fp);
	return 0;
}

int getMSB(int n) {
	return ((n & (1 << 31)) != 0);
}

int twoComplement(int n) {
	return (~n + 1);
}

void printBinary(int num) {
	char s[33];
	itoa(num, s, 2);

	printf("%s\n", s);
}

#endif
