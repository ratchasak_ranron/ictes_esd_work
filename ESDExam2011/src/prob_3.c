/*
 ============================================================================
 Name        : ESDExam2011.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exam of Embedded Software Design
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "config.h"

#ifdef RUN_3

#include <stdio.h>
#include <stdlib.h>

int string_length(const char * str) {
	int count = 0;
	while(str[count] != '\0') count++;
	return count;
}

void print_backwards(const char * str) {
	int i = string_length(str) - 1;
	while(i >= 0) printf("%c", str[i--]);
}

void print_binary(unsigned char c) {
	char bin[9];
	int i = 0;
	while(c != 0) {
		bin[i++] = '0' + (c % 2);
		c /= 2;
	}
	while(i < 8) {
		bin[i++] = '0';
	}
	bin[i] = 0;
	print_backwards(bin);
}

int main(void) {
	print_binary(16);
	return 0;
}

#endif
