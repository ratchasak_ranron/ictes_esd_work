/*
 ============================================================================
 Name        : ESDExam2011.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exam of Embedded Software Design
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "config.h"

#ifdef RUN_7_A

#include <stdio.h>
#include <stdlib.h>

void insertion_sort(int a[], int n) {
	int i, j, k, l, val;
	for(i = 1; i < n; i++) {
		val = a[i];
		for(j = 0; j < i; j++) {
			 if(val < a[j]) {
				 for(k = i; k > j; k--)
					 a[k] = a[k - 1];
				 a[j] = val;
				 break;
			 }
		}
		for(l = 0; l < n; l++)
			printf("%d ", a[l]);
		printf("\n");
	}
}

int main(void) {
	int a[8] = {0, 4, 2, 6, 7, 3, 5, 1};
	insertion_sort(a, 8);
	return 0;
}

#endif
