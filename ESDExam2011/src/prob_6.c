/*
 ============================================================================
 Name        : ESDExam2011.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exam of Embedded Software Design
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "config.h"

#ifdef RUN_6

#include <stdio.h>
#include <stdlib.h>

void matrix_vector_prod(int * P, int * M, int * V, int n) {
	int i, j;
	for(i = 0; i < n; i++) {
		P[i] = 0;
		for(j = 0; j < n; j++) {
			 P[i] += M[i * n +j] * V[j];
		}
	}
}

int main(void) {
	int i;
	int n = 3;
	int P[3];
	int M[3][3] = {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9}
	};
	int V[3] = {1, 2, 3};
	matrix_vector_prod(P, M, V, n);
	for(i = 0; i < n; i++) {
		printf("%d ", P[i]);
	}
	return 0;
}

#endif
