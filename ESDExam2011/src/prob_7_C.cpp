/*
 ============================================================================
 Name        : ESDExam2011.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exam of Embedded Software Design
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "config.h"

#ifdef RUN_7_C

#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

long comp_count_a, comp_count_a_1, comp_count_a_2, comp_count_a_3;
long comp_count_b, comp_count_b_1, comp_count_b_2, comp_count_b_3;

void insertion_sort(int a[], int n) {
	int i, j, k, val;
	for(i = 1; i < n; i++) {
		val = a[i]; comp_count_a++; comp_count_a_1++;
		for(j = 0; j < i; j++) {
			 if(val < a[j]) {
				 for(k = i; k > j; k--) {
					 a[k] = a[k - 1]; comp_count_a++; comp_count_a_2++;
				 }
				 a[j] = val; comp_count_a++; comp_count_a_3++;
				 break;
			 }
		}
	}
}

void bubble_sort(int a[], int n) {
	int i, j, val;
	for(i = 0; i < n; i++) {
		for(j = 1; j < n - i; j++) {
			if(a[j - 1] > a[j]) {
				val = a[j - 1]; comp_count_b++; comp_count_b_1++;
				a[j - 1] = a[j]; comp_count_b++; comp_count_b_2++;
				a[j] = val; comp_count_b++; comp_count_b_3++;
			}
		}
	}
}

int main(void) {
	int a[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	int aTemp[10];
	int bTemp[10];
	int n;
	int i;
	long count_sort_a = 0, count_sort_b = 0;

	for(n = 1; n <= 10; n++) {
		comp_count_a = comp_count_a_1 = comp_count_a_2 = comp_count_a_3 = 0;
		comp_count_b = comp_count_b_1 = comp_count_b_2 = comp_count_b_3 = 0;
		do {
			for(i = 0; i < n; i++) {
				aTemp[i] = a[i];
				bTemp[i] = a[i];
			}

			insertion_sort(bTemp, n);
			bubble_sort(aTemp, n);
			count_sort_a++; count_sort_b++;
		} while ( std::next_permutation(a, a + n) );
		printf("n = %d\n", n);
		printf("computational time of insertion sort : %f\n", (float) comp_count_a / count_sort_a);
		printf("computational time of insertion sort at point 1 : %f\n", (float) comp_count_a_1 / count_sort_a);
		printf("computational time of insertion sort at point 2 : %f\n", (float) comp_count_a_2 / count_sort_a);
		printf("computational time of insertion sort at point 3 : %f\n", (float) comp_count_a_3 / count_sort_a);
		printf("computational time of bubble sort : %f\n", (float) comp_count_b / count_sort_b);
		printf("computational time of bubble sort at point 1 : %f\n", (float) comp_count_b_1 / count_sort_b);
		printf("computational time of bubble sort at point 2 : %f\n", (float) comp_count_b_2 / count_sort_b);
		printf("computational time of bubble sort at point 3 : %f\n", (float) comp_count_b_3 / count_sort_b);
		printf("\n");
	}
	return 0;
}

#endif
