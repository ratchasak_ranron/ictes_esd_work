/*
 ============================================================================
 Name        : ESDExam2011.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exam of Embedded Software Design
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "config.h"

#ifdef RUN_2

#include <stdio.h>
#include <stdlib.h>

int string_length(const char * str) {
	int count = 0;
	while(str[count] != '\0') count++;
	return count;
}

void print_backwards(const char * str) {
	int i = string_length(str) - 1;
	while(i >= 0) printf("%c", str[i--]);
}

int main(void) {
	print_backwards("1234567");
	return 0;
}

#endif
