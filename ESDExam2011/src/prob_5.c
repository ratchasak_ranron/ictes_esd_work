/*
 ============================================================================
 Name        : ESDExam2011.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exam of Embedded Software Design
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "config.h"

#ifdef RUN_5

#include <stdio.h>
#include <stdlib.h>

int get_hex_value(char c) {
	if(c >= '0' && c <= '9')
		return c - '0';
	else if(c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	else if(c >= 'A' && c <= 'F')
		return c - 'A' + 10;

	return -1;
}

int hex_string_to_int(const char * hex_str) {
	int i = 2;
	int hex_value;
	int sum = 0;

	if(!(hex_str[0] == '0' && hex_str[1] == 'x') || hex_str[2] == '\0')
		return -1;
	while(hex_str[i] != '\0') {
		hex_value = get_hex_value(hex_str[i++]);
		if(hex_value == -1)
			return -1;
		sum = (sum * 16) + hex_value;
	}

	return sum;
}

int main(void) {
	printf("%d", hex_string_to_int("0xC8"));
	return 0;
}

#endif
