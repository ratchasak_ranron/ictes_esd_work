/*
 ============================================================================
 Name        : ESDExam2011.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exam of Embedded Software Design
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "config.h"

#ifdef RUN_1

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int string_length(const char * str) {
	int count = 0;
	while(str[count] != '\0') count++;
	return count;
}

int main(void) {
	printf("%d\n", string_length("1234567"));
	return 0;
}

#endif
