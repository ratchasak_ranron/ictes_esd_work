Solution
====
---

problem 1 - 6
----

already in `src/prob_x.c` where `x` is the number of problem

---
problem 7.A
----

the array values after each iteration of outermost for-loop on index `i` (result from `src/prob_7_A`) :

	i = 1 ; 0 4 2 6 7 3 5 1 
	i = 2 ; 0 2 4 6 7 3 5 1 
	i = 3 ; 0 2 4 6 7 3 5 1 
	i = 4 ; 0 2 4 6 7 3 5 1 
	i = 5 ; 0 2 3 4 6 7 5 1 
	i = 6 ; 0 2 3 4 5 6 7 1 
	i = 7 ; 0 1 2 3 4 5 6 7 
---
problem 7.B
----

for each `i` loop, try to insert number of index `i` to the proper position of sorted elements `0` to `i - 1`

---
problem 7.C
----
assuming the average computation time of read/write on a[] is

_average computation time of all permutation number (not including repeat number in the sorting list)_

from `src/prob_7_C.cpp` (use c++ algorithm library for permutation)

the result is :

**Total**

| n     | insertion  | bubble    |
|:-----:| ----------:| ---------:|
| 1     |  0.000000  |  0.000000 |
| 2     |  1.333333  |  1.000000 |
| 3     |  3.111111  |  3.000000 |
| 4     |  5.757576  |  6.545455 |
| 5     |  9.189542  | 11.764706 |
| 6     | 13.237113  | 18.556701 |
| 7     | 17.820396  | 26.849315 |
| 8     | 22.920771  | 36.628382 |
| 9     | 28.535451  | 47.897574 |
| 10    | 34.663095  | 60.661040 |

**Execute Point 1**

| n     | insertion  | bubble    |
|:-----:| ----------:| ---------:|
| 1     |  0.000000  |  0.000000 |
| 2     |  0.666667  |  0.333333 |
| 3     |  1.333333  |  1.000000 |
| 4     |  2.181818  |  2.181818 |
| 5     |  3.137255  |  3.921569 |
| 6     |  4.123711  |  6.185567 |
| 7     |  5.114155  |  8.949772 |
| 8     |  6.104730  | 12.209461 |
| 9     |  7.095937  | 15.965858 |
| 10    |  8.088139  | 20.220347 |

**Execute Point 2**

| n     | insertion  | bubble    |
|:-----:| ----------:| ---------:|
| 1     |  0.000000  |  0.000000 |
| 2     |  0.333333  |  0.333333 |
| 3     |  1.000000  |  1.000000 |
| 4     |  2.181818  |  2.181818 |
| 5     |  3.921569  |  3.921569 |
| 6     |  6.185567  |  6.185567 |
| 7     |  8.949772  |  8.949772 |
| 8     | 12.209461  | 12.209461 |
| 9     | 15.965858  | 15.965858 |
| 10    | 20.220347  | 20.220347 |

**Execute Point 3**

| n     | insertion  | bubble	 |
|:-----:| ----------:| ---------:|
| 1     |  0.000000  |  0.000000 |
| 2     |  0.333333  |  0.333333 |
| 3     |  0.777778  |  1.000000 |
| 4     |  1.393939  |  2.181818 |
| 5     |  2.130719  |  3.921569 |
| 6     |  2.927835  |  6.185567 |
| 7     |  3.756469  |  8.949772 |
| 8     |  4.606580  | 12.209461 |
| 9     |  5.473656  | 15.965858 |
| 10    |  6.354609  | 20.220347 |

from the result can be sumarize as following

* insertion sort is faster than bubble sort from `Total` table.
* execution at point 2 of both is the same.
* execution at point 1 and 3 of insertion sort is grow slower than bubble sort.
* execution at point 1 of insertion sort is grow slower than point 3 of the same sorting. 
