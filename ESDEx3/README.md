Exercise 3
===========

Name : Ratchasak Ranron

Student ID : 56-2204-0664

---

Files description
------------------

In attached file, I include 2 folders named "src", "input" and "output"

* the "src" folder contains .c source file named "ex3_x.c" where x is the number of problem.
* the "input" folder contains .bmp file which is the input file to modify for all problem.
* the "output" folder contains .bmp output file named "ex3_x.bmp" where ex3_x is the name of executed source file.

Note
-----

I used "Eclipse" as an editor. I have to create a project (contains all problem solution) which can have only one main function.
So I used the "ifdef" pre-processor to select which main function I need to execute and control by change defined variable RUN_X in "config.h",
where X is the number of problem. for example if I want to run problem 7, I just change it to RUN_7.
