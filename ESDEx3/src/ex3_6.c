/*
 ============================================================================
 Name        : ex3_6.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : In the color quantization example, each colormap value was
 	 simply calculated as the average of min/max range values. Consider
 	 improving the quantized image quality by setting the colormap value
 	 as the average RGB values of all pixels that map to that colormap
 ============================================================================
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "./config.h"
#include "bmp.h"

#ifdef RUN_6

#define INPUT_IMG "./input/duck.bmp"
#define OUTPUT_IMG "./output/ex3_6.bmp"

void BMPIMG_create_colormap(BMPIMG * img, int RQ, int GQ, int BQ) {
	int i, j, k;
	if(RQ * GQ * BQ > 256 || RQ <= 0 || GQ <= 0 || BQ <= 0){
		printf("Q-levels out of range!!! (%d * %d * %d = %d)\n",
			RQ, GQ, BQ, RQ * GQ * BQ);
		return;
	}

	//=========== prepare data for average
	int sR[256], sG[256], sB[256];
	// initial array for sum
	for(i = 0; i < 256; i++) {
		sR[i] = 0;
		sG[i] = 0;
		sB[i] = 0;
	}

	// sR[x] is number of color x exist in all pixel
	PIX * pix;
	for(i = 0, pix = img->bitmap; i < img->size_xy; i ++, pix ++){
		sR[pix->R]++;
		sG[pix->G]++;
		sB[pix->B]++;
	}
	//==========================

	int r;
	for(i = 0; i < RQ; i ++){
		int r_min = (i * 256 + RQ - 1) / RQ;
		int r_max = ((i + 1) * 256 + RQ - 1) / RQ - 1;
		for(j = 0; j < GQ; j ++){
			int g_min = (j * 256 + GQ - 1) / GQ;
			int g_max = ((j + 1) * 256 + GQ - 1) / GQ - 1;
			for(k = 0; k < BQ; k ++){
				int b_min = (k * 256 + BQ - 1) / BQ;
				int b_max = ((k + 1) * 256 + BQ - 1) / BQ - 1;
				int cid = i * GQ * BQ + j * BQ + k;

				int sumR = 0, cR = 0;
				int sumG = 0, cG = 0;
				int sumB = 0, cB = 0;

				for(r = r_min; r <= r_max; r++) {
					sumR += (sR[r] * r);
					cR += sR[r];
				}
				for(r = g_min; r <= g_max; r++) {
					sumG += (sG[r] * r);
					cG += sG[r];
				}
				for(r = b_min; r <= b_max; r++) {
					sumB += (sB[r] * r);
					cB += sB[r];
				}


				img->colormap[cid].R = sumR / cR;
				img->colormap[cid].G = sumG / cG;
				img->colormap[cid].B = sumB / cB;
				printf("%3d: R(%3d,%3d,%3d) G(%3d,%3d,%3d) B(%3d,%3d,%3d)\n", cid,
						img->colormap[cid].R, r_min, r_max,
						img->colormap[cid].G, g_min, g_max,
						img->colormap[cid].B, b_min, b_max);
			}
		}
	}
}

void BMPIMG_convert_to_colormap(BMPIMG * img, int RQ, int GQ, int BQ) {
	int i;
	PIX * pix;
	for(i = 0, pix = img->bitmap; i < img->size_xy; i ++, pix ++){
		pix->I = (pix->R * RQ / 256) * GQ * BQ +
			(pix->G * GQ / 256) * BQ +
			(pix->B * BQ / 256);
	}
	img->out_pix_bits = 8;
	img->colormap_size = 256;
}

int main(int avgc, char* argv[]) {

	BMPIMG img;
	BMPIMG_initialize(&img);

	if(!BMPIMG_open(&img, INPUT_IMG)){
		printf("open file <%s> failed\n", INPUT_IMG);
		return 0;
	}

	int RQ = 6, GQ = 7, BQ = 6;
	BMPIMG_create_colormap(&img, RQ, GQ, BQ);
	BMPIMG_convert_to_colormap(&img, RQ, GQ, BQ);

	BMPIMG_save(&img, OUTPUT_IMG);
	BMPIMG_destroy(&img);
	return 0;
}

#endif
