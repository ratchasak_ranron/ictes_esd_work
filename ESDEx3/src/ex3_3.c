/*
 ============================================================================
 Name        : ex3_3.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Rotate the image 90 degrees
 ============================================================================
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "./config.h"
#include "bmp.h"

#ifdef RUN_3

#define INPUT_IMG "./input/duck.bmp"
#define OUTPUT_IMG "./output/ex3_3.bmp"

PIX * get_pix(BMPIMG * img, int i, int j) {
	return (img->bitmap + (i * img->size_x) + j);
}

void swapNum(int* num1, int* num2) {
	int temp;
	temp = *num1;
	*num1 = *num2;
	*num2 = temp;
}

void copy_pix(PIX* sPix, PIX* dPix) {
	dPix->R = sPix->R;
	dPix->G = sPix->G;
	dPix->B = sPix->B;
	dPix->I = sPix->I;
}

void BMPIMG_rotate_90degree(BMPIMG * img)
{
	int y, x, i;
	PIX* bufferPix = (PIX *) malloc(sizeof(PIX) * img->size_xy);

	// copy pix to buffer
	PIX* pix = img->bitmap;
	for(i = 0; i < img->size_xy; i++) {
		copy_pix(&(pix[i]), &(bufferPix[i]));
	}

	swapNum(&(img->size_y), &(img->size_x));

	// copy buffer to changed size image.
	int k = 0;
	for(x = img->size_x - 1; x >= 0; x--) {
		for(y = 0; y < img->size_y; y ++) {
			copy_pix(&(bufferPix[k++]), get_pix(img, y, x));
		}
	}

	free(bufferPix);
}

int main(int avgc, char* argv[]) {

	BMPIMG img;
	BMPIMG_initialize(&img);

	if(!BMPIMG_open(&img, INPUT_IMG)){
		printf("open file <%s> failed\n", INPUT_IMG);
		return 0;
	}

	BMPIMG_rotate_90degree(&img);

	BMPIMG_save(&img, OUTPUT_IMG);
	BMPIMG_destroy(&img);
	return 0;
}

#endif
