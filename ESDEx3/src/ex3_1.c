/*
 ============================================================================
 Name        : ex3_1.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Generate a �negative� greyscale image
			   A �negative� image is an image where white and black are reversed
 ============================================================================
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "./config.h"
#include "bmp.h"

#ifdef RUN_1

#define INPUT_IMG "./input/duck.bmp"
#define OUTPUT_IMG "./output/ex3_1.bmp"

void BMPIMG_negative_greyscale(BMPIMG * img)
{
	int i;
	PIX * pix;
	/* set greyscale colormap */
	for(i = 0; i < 256; i ++){
		img->colormap[i].B = i;
		img->colormap[i].G = i;
		img->colormap[i].R = i;
	}
	img->out_pix_bits = 8;
	img->colormap_size = 256;
	pix = img->bitmap;
	for(i = 0; i < img->size_xy; i ++, pix ++){
#if 0	/* definition of Y (luminance) */
		pix->I = (unsigned char) (pix->R * 0.299 + pix->G * 0.587 + pix->B * 0.114);
#else	/* simple average of RGB */
		pix->I = (pix->R + pix->G + pix->B) / 3;
#endif
		pix->I = 255 - pix->I;	// negative greyscale
	}
}

int main(int avgc, char* argv[]) {

	BMPIMG img;
	BMPIMG_initialize(&img);

	if(!BMPIMG_open(&img, INPUT_IMG)){
		printf("open file <%s> failed\n", INPUT_IMG);
		return 0;
	}

	BMPIMG_negative_greyscale(&img);

	BMPIMG_save(&img, OUTPUT_IMG);
	BMPIMG_destroy(&img);
	return 0;
}

#endif
