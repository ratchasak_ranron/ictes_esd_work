/*
 ============================================================================
 Name        : ex3_5.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Enlarge the image width by 1.5 times
 ============================================================================
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "./config.h"
#include "bmp.h"

#ifdef RUN_5

#define INPUT_IMG "./input/duck.bmp"
#define OUTPUT_IMG "./output/ex3_5.bmp"

PIX * get_pix(BMPIMG * img, int i, int j) {
	return (img->bitmap + (i * img->size_x) + j);
}

void copy_pix(PIX* sPix, PIX* dPix) {
	dPix->R = sPix->R;
	dPix->G = sPix->G;
	dPix->B = sPix->B;
	dPix->I = sPix->I;
}

void BMPIMG_enlarge_width_1_5(BMPIMG * img)
{
	int y, x, i;
	PIX* bufferPix = (PIX *) malloc(sizeof(PIX) * img->size_xy);

	// copy pix to buffer
	PIX* pix = img->bitmap;
	for(i = 0; i < img->size_xy; i++) {
		copy_pix(&(pix[i]), &(bufferPix[i]));
	}

	img->size_x = (int)(img->size_x * 1.5);
	img->size_xy = img->size_x * img->size_y;

	// destroy old bitmap and allocate new extended bitmap
	free(img->bitmap);
	img->bitmap = (PIX *) malloc(sizeof(PIX) * img->size_xy);

	PIX avg;
	PIX* pix1;
	PIX* pix2;
	int k = 0;
	for(y = 0; y < img->size_y; y ++) {
		for(x = 0; x < img->size_x; x++) {
			if((x - 1) % 3 == 0) {
				pix1 = &(bufferPix[k - 1]);
				pix2 = &(bufferPix[k]);

				avg.R = (pix1->R + pix2->R) / 2;
				avg.G = (pix1->G + pix2->G) / 2;
				avg.B = (pix1->B + pix2->B) / 2;
				avg.I = (pix1->I + pix2->I) / 2;
				copy_pix(&(avg), get_pix(img, y, x));
			}
			else {
				copy_pix(&(bufferPix[k++]), get_pix(img, y, x));
			}
		}
	}

	free(bufferPix);
}

int main(int avgc, char* argv[]) {

	BMPIMG img;
	BMPIMG_initialize(&img);

	if(!BMPIMG_open(&img, INPUT_IMG)){
		printf("open file <%s> failed\n", INPUT_IMG);
		return 0;
	}

	BMPIMG_enlarge_width_1_5(&img);

	BMPIMG_save(&img, OUTPUT_IMG);
	BMPIMG_destroy(&img);
	return 0;
}

#endif
