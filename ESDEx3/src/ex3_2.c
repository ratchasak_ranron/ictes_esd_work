/*
 ============================================================================
 Name        : ex3_2.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Flip the image upside down
 ============================================================================
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "./config.h"
#include "bmp.h"

#ifdef RUN_2

#define INPUT_IMG "./input/duck.bmp"
#define OUTPUT_IMG "./output/ex3_2.bmp"

PIX * get_pix(BMPIMG * img,int i, int j) {
	PIX * pix;
	pix = img->bitmap;
	return (pix + (i * img->size_x) + j);
}

void swap_pix(PIX* pix1, PIX* pix2) {
	unsigned char tempR, tempG, tempB, tempI;
	tempR = pix1->R;
	tempG = pix1->G;
	tempB = pix1->B;
	tempI = pix1->I;
	pix1->R = pix2->R;
	pix1->G = pix2->G;
	pix1->B = pix2->B;
	pix1->I = pix2->I;
	pix2->R = tempR;
	pix2->G = tempG;
	pix2->B = tempB;
	pix2->I = tempI;
}

void BMPIMG_flip_upsidedown(BMPIMG * img)
{
	int i, j;

	PIX * upPix;
	PIX * downPix;
	int halfHeight = (img->size_y/2);
	for(i = 0; i < halfHeight; i ++){
		for(j = 0; j < img->size_x; j++) {
			upPix = get_pix(img, i, j);
			downPix = get_pix(img, (img->size_y - i - 1), j);
			swap_pix(upPix, downPix);
		}
	}
}

int main(int avgc, char* argv[]) {

	BMPIMG img;
	BMPIMG_initialize(&img);

	if(!BMPIMG_open(&img, INPUT_IMG)){
		printf("open file <%s> failed\n", INPUT_IMG);
		return 0;
	}

	BMPIMG_flip_upsidedown(&img);

	BMPIMG_save(&img, OUTPUT_IMG);
	BMPIMG_destroy(&img);
	return 0;
}

#endif
