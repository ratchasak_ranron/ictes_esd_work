/*
 ============================================================================
 Name        : ex3_4.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Shrink the image width to one half
 ============================================================================
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "./config.h"
#include "bmp.h"

#ifdef RUN_4

#define INPUT_IMG "./input/duck.bmp"
#define OUTPUT_IMG "./output/ex3_4.bmp"

PIX * get_pix(BMPIMG * img, int i, int j) {
	return (img->bitmap + (i * img->size_x) + j);
}

void copy_pix(PIX* sPix, PIX* dPix) {
	dPix->R = sPix->R;
	dPix->G = sPix->G;
	dPix->B = sPix->B;
	dPix->I = sPix->I;
}

void BMPIMG_shrink_width_onehalf(BMPIMG * img)
{
	int y, x, i;
	PIX* bufferPix = (PIX *) malloc(sizeof(PIX) * img->size_xy);

	// copy pix to buffer
	PIX* pix = img->bitmap;
	for(i = 0; i < img->size_xy; i++) {
		copy_pix(&(pix[i]), &(bufferPix[i]));
	}

	img->size_x = img->size_x / 2;
	img->size_xy = img->size_x * img->size_y;

	PIX avg;
	PIX* pix1;
	PIX* pix2;
	int k = 0;
	for(y = 0; y < img->size_y; y ++) {
		for(x = 0; x < img->size_x; x++) {
			pix1 = &(bufferPix[k++]);
			pix2 = &(bufferPix[k++]);
			avg.R = (pix1->R + pix2->R) / 2;
			avg.G = (pix1->G + pix2->G) / 2;
			avg.B = (pix1->B + pix2->B) / 2;
			avg.I = (pix1->I + pix2->I) / 2;
			copy_pix(&(avg), get_pix(img, y, x));
		}
	}

	free(bufferPix);
}

int main(int avgc, char* argv[]) {

	BMPIMG img;
	BMPIMG_initialize(&img);

	if(!BMPIMG_open(&img, INPUT_IMG)){
		printf("open file <%s> failed\n", INPUT_IMG);
		return 0;
	}

	BMPIMG_shrink_width_onehalf(&img);

	BMPIMG_save(&img, OUTPUT_IMG);
	BMPIMG_destroy(&img);
	return 0;
}

#endif
