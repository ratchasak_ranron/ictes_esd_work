Exercise 2
===========

Name : Ratchasak Ranron

Student ID : 56-2204-0664

---

Files description
------------------

In attached file, I include 2 folders named "src" and "output"

* the "src" folder contain .c source file named "ex2_x.c" where x is the number of problem.
* the "output" folder contain .txt output file named "ex2_x.txt" where ex1_x is the name of executed source.

Note
-----

I used "Eclipse" as an editor. I have to create a project (contains all problem solution) which can have only one main function.
So I used the "ifdef" pre-processor to select which main function I need to execute and control by change defined variable RUN_X in "config.h",
where X is the number of problem. for example if I want to run problem 7, I just change it to RUN_7.
