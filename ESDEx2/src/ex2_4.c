/*
 ============================================================================
 Name        : ex2_4.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Write a sorting program using �merge sort� algorithm
 	 	 	   on every array size.
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "./config.h"

#ifdef RUN_4

#define OUTPUT_FILE "./output/ex2_4.txt"
FILE * fp;

void merge_sort(int a[], int N);

int main(int argc, char* argv[]) {
	fp = fopen(OUTPUT_FILE, "a+");
	int i;
	int numAryLength = argc - 1;
	int num[numAryLength];

	// print sequence of number and assign the numbers to num array
	fprintf(fp, "Inputed numbers : ");
	for(i = 0; i < numAryLength; i++) {
		num[i] = atoi(argv[i + 1]);
		if(i == 0) {
			fprintf(fp, "%d", num[i]);
		}
		else fprintf(fp, " , %d", num[i]);
	}
	fprintf(fp, "\n");

	// sort using merge sort.
	merge_sort(num, numAryLength);

	// print sorted numbers.
	fprintf(fp, "Sorted numbers : ");
	for(i = 0; i < numAryLength; i++) {
		if(i == 0) {
			fprintf(fp, "%d", num[i]);
		}
		else fprintf(fp, " , %d", num[i]);
	}
	fprintf(fp, "\n");

	fclose(fp);
	return 0;
}

void merge_sort(int a[], int N) {
	int k;
	int maxK = ceil((float)log(N)/log(2));
	for(k = 0; k < maxK; k++) {
		int mOff; // merging offset
		int blockSize = pow(2, k + 1); // block of merging number
		int halfBlockSize = (blockSize/2);
		for(mOff = 0; mOff < N; mOff += blockSize) {
			// sort numbers in the block to temp.
			int temp[blockSize];
			int restNumber = N - mOff;
			int left, mid, right;
			if(restNumber < blockSize) {
				left = mOff;
				mid = N - (N % (int)pow(2, k)); // mid is offset of last part of previous k
				right = mOff + restNumber - 1;
			} else {
				left = mOff;
				mid = mOff + halfBlockSize;
				right = mOff + blockSize - 1;
			}

			int i = left;
			int j = mid;
			int runner = 0;
			while(i < mid && j <= right) {
				if(a[i] < a[j]) {
					temp[runner++] = a[i++];
				}
				else if(a[j] < a[i]) {
					temp[runner++] = a[j++];
				}
				else if(a[i] == a[j]) {
					temp[runner++] = a[i++];
					temp[runner++] = a[j++];
				}
			}

			// copy the rest of left part or right part
			if(i < mid) {
				while(i < mid) temp[runner++] = a[i++];
			}
			else if(j <= right) {
				while(j <= right) temp[runner++] = a[j++];
			}

			// copy sorted numbers
			int m;
			for(m = 0; m < runner; m++) {
				a[m + mOff] = temp[m];
			}
		}
	}
}

#endif
