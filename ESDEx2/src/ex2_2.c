/*
 ============================================================================
 Name        : ESDEx2.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Write a program that prints the �median� value
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "./config.h"

#ifdef RUN_2

#define OUTPUT_FILE "./output/ex2_2.txt"
FILE * fp;

void swap(int* a0, int* a1);
int partition(int a[], int left, int right);
void quick_sort_median(int a[], int left, int right, int mid1, int mid2);
void quick_sort(int a[], int left, int right);

int main(int argc, char* argv[]) {
	fp = fopen(OUTPUT_FILE, "a+");
	int i;
	int isPrintFirst = 1;
	int numAryLength = argc - 1;
	int num[numAryLength];

	// print sequence of number and assign the numbers to num array
	fprintf(fp, "Inputed numbers : ");
	for(i = 0; i < numAryLength; i++) {
		num[i] = atoi(argv[i + 1]);
		if(isPrintFirst) {
			fprintf(fp, "%d", num[i]);
			isPrintFirst = 0;
		}
		else fprintf(fp, " , %d", num[i]);
	}

	// find mid1 and mid2 index, if number of sequence is odd mid2 set to -1 (not use)
	int mid1, mid2 = -1;
	if(numAryLength % 2 == 1) {
		mid1 = numAryLength / 2;
	} else {
		mid1 = (numAryLength / 2) - 1;
		mid2 = (numAryLength / 2);
	}

	// sort median numbers
	quick_sort_median(num, 0, numAryLength - 1, mid1, mid2);

	float median;
	if(mid2 == -1) median = num[mid1];
	else median = (float)(num[mid1] + num[mid2]) / 2;

	fprintf(fp, "\nMedian : %.2f\n", median);

	fclose(fp);
	return 0;
}

void swap(int* a0, int* a1) {
	int t = *a0;
	*a0 = *a1;
	*a1 = t;
}

int partition(int a[], int left, int right) {
	int pivot = a[right];
	int i = left, k = right;

	while(i < k) {
		while(i < k && a[i] < pivot) i++;
		while(i < k && a[k] >= pivot) k--;
		if(i < k) swap(&a[i], &a[k]);
	}
	if(right > k) swap(&a[right], &a[k]);
	return k;
}

void quick_sort_median(int a[], int left, int right, int mid1, int mid2) {
	if(left < right) {
		int pivot_pos = partition(a, left, right);

		// do quick sort of left and right part if mid1 and mid2 is in the range
		if((mid1 >= left && mid1 <= pivot_pos - 1)
				|| (mid2 != -1 && mid2 >= left && mid2 <= pivot_pos - 1)) {
			quick_sort_median(a, left, pivot_pos - 1, mid1, mid2);
		}
		if((mid1 >= pivot_pos + 1 && mid1 <= right)
						|| (mid2 != -1 && mid2 >= pivot_pos + 1 && mid2 <= right)) {
			quick_sort_median(a, pivot_pos + 1, right, mid1, mid2);
		}
	}
}

void quick_sort(int a[], int left, int right) {
	if(left < right) {
		int pivot_pos = partition(a, left, right);
		quick_sort(a, left, pivot_pos - 1);
		quick_sort(a, pivot_pos + 1, right);
	}
}
#endif
