/*
 ============================================================================
 Name        : ex2_1.c
 Author      : Ratchasak Ranron, 5622040664
 Version     :
 Copyright   : This program is a exercise of Embedded Software Design
 Description : Write a program that sorts words in dictionary order
 	 	 	   (words should be given from the command arguments)
 ============================================================================
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "./config.h"

#ifdef RUN_1

#define OUTPUT_FILE "./output/ex2_1.txt"
FILE * fp;
int compareWord(char* word1, char* word2);

int main(int avgc, char* argv[]) {
	char** strAry = &argv[1];	// define own array which strAry[0] is first word
	int strAryLength = avgc - 1;
	int i, j;

	fp = fopen(OUTPUT_FILE, "a+");

	fprintf(fp, "Sequence of words : ");
	for(i = 0; i < strAryLength; i++) {
		fprintf(fp, "%s ", strAry[i]);
	}
	fprintf(fp, "\n");

	// sort by bubble sort
	for(i = 0; i < strAryLength - 1; i++) {
		for(j = 0; j < strAryLength - i - 1; j++) {
			// if word1 should be after word2
			if(compareWord(strAry[j], strAry[j + 1]) == -1) {
				//swap word
				char* temp = strAry[j];
				strAry[j] = strAry[j + 1];
				strAry[j + 1] = temp;
 			}
		}
	}

	fprintf(fp, "Sorted words : ");
	for(i = 0; i < strAryLength; i++) {
		fprintf(fp, "%s ", strAry[i]);
	}
	fprintf(fp, "\n");

	fclose(fp);
	return 0;
}

/**
 * @brief compare which word should come before
 *
 * @param word1 string
 * @param word2 string
 * @return
 * 		1 if word1 should be before word2
 * 		0 if word1 is same as word2
 * 	   -1 if word1 should be after word2
 */
int compareWord(char* word1, char* word2) {
	int i;
	for(i = 0; ; i++) {
		if(tolower(word1[i]) < tolower(word2[i])) {
			return 1;
		}
		else if(tolower(word1[i]) > tolower(word2[i])) {
			return -1;
		}

		if(word1[i] == '\0' && word2[i] == '\0') {
			return 0;
		}
	}

	return 0;
}

#endif
